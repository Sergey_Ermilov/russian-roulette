﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMechanic : MonoBehaviour
{
    [SerializeField] private RotateClip rotClip;
    [SerializeField] private RotateGun rotateGun;
    [SerializeField] private Shoot shootGun;
    [SerializeField] private GameObject title;
    [SerializeField] private AudioSource roulete;

    private InputSystem _inputSystem;
    private void Start()
    {
        _inputSystem = gameObject.GetComponent<InputSystem>();

        _inputSystem.IsSwipe += () =>
        {
            rotClip.OnRotate = !rotClip.OnRotate;
            rotateGun.OnLookAtCam = true;
            roulete.Play();
        };
        rotateGun.GunInCam += () =>
        {
            rotateGun.OnLookAtCam = false;
            rotClip.OnRotate = !rotClip.OnRotate;
            title.SetActive(true);
        };
        _inputSystem.IsVolume += () =>
        {
            ShootMaker();
        };
    }

    public void ShootMaker()
    {
        float rdm  =  Random.Range(1, 100);
        if(rdm <= 16.7f)
            shootGun.PlayShoot();
        else
            shootGun.PlayNoShoot();
    }
}
