﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class InputSystem : MonoBehaviour
{
    public event Action IsSwipe;
    public event Action IsVolume;

    private float volumePhone;

    private void Start()
    {
        volumePhone = AudioListener.volume;
    }

    private void Update()
    {
        foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Moved)
            {
                IsSwipe.Invoke();
            }
        }

        if (Math.Abs(volumePhone - AudioListener.volume) > 0)
        {
            volumePhone = AudioListener.volume;
            IsVolume.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            IsSwipe.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            IsVolume.Invoke();
        }

    }
}
