﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGun : MonoBehaviour
{
    public bool OnLookAtCam { get; set; }
    public Action GunInCam;
    
   [SerializeField] private Transform gun;
   [SerializeField] private Transform cam;
    void Update()
    {
        if(OnLookAtCam)
            LookAtCam();
    }

    private void LookAtCam()
    {
        var targetRotation = Quaternion.LookRotation (
            cam.position- new Vector3(0, 1.25f, 0) - gun.position ,
            Vector3.up);
                               
                               
        gun.rotation = Quaternion.RotateTowards(gun.rotation,targetRotation,Time.deltaTime * 130);
        
        if (gun.transform.rotation == targetRotation)
            GunInCam.Invoke();
    }
}
