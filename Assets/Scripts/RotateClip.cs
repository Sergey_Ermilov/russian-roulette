﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateClip : MonoBehaviour
{
    public bool OnRotate { get; set; }
    
    [SerializeField] private Transform clip;

    private void Update()
    {
        if(OnRotate)
            Rotate();
    }


    private void Rotate()
    {
        OnRotate = true;
        clip.Rotate(Vector3.up *3);
    }
}
