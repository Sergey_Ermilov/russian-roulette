﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] private AudioSource shootSound;
    [SerializeField] private AudioSource noShootSound;

    public void PlayShoot()
    {
        shootSound.Play();
    }
    public void PlayNoShoot()
    {
        noShootSound.Play();
    }
}
